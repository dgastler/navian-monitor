#!/usr/bin/env python3

import paho.mqtt.client as mqtt #import the client1

from picamera.array import PiRGBArray
from picamera import PiCamera

import time

import cv2

import numpy as np

import pidfile
import daemon
import signal
running = True
PIDFILE_PATH = '/var/run/uio_daemon.pid'                    # PID File Path (Configured in service file)
DUMP_PATH = '/var/www/html/'


import datetime

status_image_size       = (400,100)
value_image_size        = (400,200)
value_status_image_size = (400,100)

def rescaleImage(image,x,y):
    dim = (x, y)
    # resize image
    return cv2.resize(image, dim, interpolation = cv2.INTER_AREA)

def scanStatus(image):
    statusValues = [0,0,0,0]
    fractions = [0,0,0,0]
    box = (((20 ,0),(110,100)),
           ((110,0),(200,100)),
           ((200,0),(290,100)),
           ((290,0),(380,100)))
    img_copy=image.copy()
    for pos in range(0,4):        
        sub_image = img_copy[box[pos][0][1]:box[pos][1][1],
                             box[pos][0][0]:box[pos][1][0]]
        white_pix = (cv2.sumElems(sub_image)[0]/255.0 +
                     cv2.sumElems(sub_image)[1]/255.0 +
                     cv2.sumElems(sub_image)[2]/255.0)
        box_pixel_count = (box[pos][1][1] - box[pos][0][1])*(box[pos][1][0] - box[pos][0][0])
        
        black_pix = box_pixel_count - white_pix
        color = (0,0,255)
        fraction=black_pix/(black_pix+white_pix)
        fractions[pos] = fraction
        if fraction > 0.25:
            statusValues[pos] = 1
        image = cv2.rectangle(image,box[pos][0],box[pos][1],(255,0,0),1)
    cv2.imwrite(DUMP_PATH+"/navian_status_maked.jpg",image)
    try:
        f = open(DUMP_PATH+'/navian_status.txt',mode='w')
        for val in fractions:
            f.write(str(val))
            f.write(" ")
        f.write("\n")
        for val in statusValues:
            f.write(str(val))
            f.write(" ")
        f.write("\n")
    finally:
        f.close()
    return statusValues

def processImage(img,threshold,angle):
    # grab the dimensions of the image and calculate the center of the
    # image
    (h, w) = img.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    # rotate our image by 45 degrees around the center of the image
    M = cv2.getRotationMatrix2D((cX, cY), angle, 1.0)
    img = cv2.warpAffine(img, M, (w, h))

    cv2.imwrite(DUMP_PATH+"/navian_img.jpg",img)
#    status_rng    = (( 300, 700),( 700,2100))
#    value_rng     = (( 650,1300),( 700,2100))
#    value_sel_rng = ((1200,1500),( 700,2100))
    status_rng    = (( 700,2100),( 350, 650))
    value_rng     = (( 700,2100),( 650,1300))
    value_sel_rng = (( 700,2100),(1200,1500))
    
    
    status_image       = rescaleImage(img[ status_rng[1][0]:status_rng[1][1]  ,  status_rng[0][0]:status_rng[0][1]] ,
                                      status_image_size[0]      ,
                                      status_image_size[1]      )
    value_image        = rescaleImage(img[ value_rng[1][0]:value_rng[1][1]  ,  value_rng[0][0]:value_rng[0][1]] ,
                                      value_image_size[0]       ,
                                      value_image_size[1]       )
    value_select_image = rescaleImage(img[ value_sel_rng[1][0]:value_sel_rng[1][1]  ,  value_sel_rng[0][0]:value_sel_rng[0][1]] ,
                                      value_status_image_size[0],
                                      value_status_image_size[1])

    cv2.imwrite(DUMP_PATH+"/navian_status.jpg",status_image)
    cv2.imwrite(DUMP_PATH+"/navian_value.jpg",value_image)
    cv2.imwrite(DUMP_PATH+"/navian_value_select.jpg",value_select_image)
    
    status_image       = 255 - cv2.cvtColor(status_image, cv2.COLOR_BGR2GRAY)
    ret,status_image   = cv2.threshold(status_image,threshold,255,cv2.THRESH_BINARY)

    cv2.imwrite(DUMP_PATH+"/navian_status2.jpg",status_image)
    current_status = scanStatus(status_image)
#    print("{}: {} {} {} {}".format(time.time(),
#                                   current_status[3],
#                                   current_status[2],
#                                   current_status[1],
#                                   current_status[0]))
    
    value_image        = 255 - cv2.cvtColor(value_image, cv2.COLOR_BGR2GRAY)
    ret,value_image    = cv2.threshold(value_image,threshold,255,cv2.THRESH_BINARY)
    cv2.imwrite(DUMP_PATH+"/navian_value2.jpg",value_image)
    
    value_select_image = 255 - cv2.cvtColor(value_select_image, cv2.COLOR_BGR2GRAY)
    ret,value_select_image    = cv2.threshold(value_select_image,threshold,255,cv2.THRESH_BINARY)
    cv2.imwrite(DUMP_PATH+"/navian_value_select2.jpg",value_select_image)


    return current_status





def main(broker_address="192.168.2.10"):
    global running

    #signal handler (here because python sucks)
    def signal_handler(signum, frame):  
      global running
      print("Handler got"+str(signum))
      print("Running is "+str(running))
      running=False


    client = mqtt.Client("navien-monitor") #create new instance


    #main
    print("Setting up MQTT")
    client.username_pw_set("sdr-monitor","passwd")
    
    
    client.connect(broker_address) #connect to broker
    
    print("Setting up camera")
    # initialize the camera and grab a reference to the raw camera capture
    camera = PiCamera()
    camera.resolution=(2592,1944)
    #camera.resolution=(1944,2592)
    # allow the camera to warmup
    time.sleep(2)
    
    
    client.loop_start() #start the loop
    
    period = 1
    now = time.time()
    next = now+period
    
    update_period = 300
    next_update = now + update_period
    
    topic=["navien/heat","navien/error","navien/burner","navien/hot_water"]
    name=["heating","error","burner_on","hot_water"]
    
    last_status = [0,0,0,0]
    
    print("starting main loop")
    update=True
    while running:
        
        # grab an image from the camera
        rawCapture = PiRGBArray(camera)
        camera.capture(rawCapture, format="bgr")
        img = rawCapture.array
        cv2.imwrite(DUMP_PATH+"/navian_raw.jpg",img)    
        
        current_status = processImage(img,200,91.5)

        
        for i in range(0,4):
            update = update or (current_status[i] != last_status[i]) 
    
        last_status = current_status
        now = time.time();
        update = update or (now > next_update)
    
        if update:
            print("Sending "+str(now)+" "+str(current_status))
            next_update = now + update_period
            for i in range(0,4):
                client.publish(topic[i],"{\""+name[i]+"\": "+str(current_status[i])+"}")#publish



        #dump status html
        with open(DUMP_PATH+"/index.html","w") as html_file:
            html_file.write("<!DOCTYPE html>\n")
            html_file.write("<html>\n")
            html_file.write("<body>\n")
            html_file.write("<h1> Date:"+str(datetime.datetime.now())+"</h1>\n")
            html_file.write("<img src=\"navian_raw.jpg\">\n")
            html_file.write("<img src=\"navian_img.jpg\">\n")
            html_file.write("<img src=\"navian_status.jpg\">\n")
            html_file.write("<img src=\"navian_value.jpg\">\n")
            html_file.write("<img src=\"navian_value_select.jpg\">\n")
            html_file.write("<img src=\"navian_status2.jpg\">\n")
            html_file.write("<img src=\"navian_value2.jpg\">\n")
            html_file.write("<img src=\"navian_value_select2.jpg\">\n")
            html_file.write("</body>\n")
            html_file.write("</html>\n")

            
            
        while now < next:
            time.sleep((next-now)*0.8)
            now= time.time()
    
        next = next + period
        update=False
    
    client.loop_stop()
    client.disconnect()


    
main()
if __name__ == "__main__":
     with daemon.DaemonContext(working_directory="/tmp/", pidfile=pidfile.PIDFile(PIDFILE_PATH)) as context:
      main()
